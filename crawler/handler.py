from bs4 import BeautifulSoup
from requests import get
from abc import ABC, abstractmethod

class Base(ABC):
    
    @staticmethod
    def get_html_content(url):
        try:
            return get(url).text
        except:
            print("ERROR: ", url)
            return False


class LinkCrawler(Base):
    
    def __init__(self, url, start_link=None, attr_tag=None):
        self.url = url
        self.start_link = start_link
        self.attr_tag = attr_tag

    def crawler(self, html_content):
        soup = BeautifulSoup(html_content, 'html.parser')
        content = soup.find_all("a", attrs=self.attr_tag)
        href = []
        if self.start_link is not None:
            for a_tag in content:
                href.append(self.start_link+a_tag.get("href"))
        else:
            for a_tag in content:
                href.append(a_tag.get("href"))
        return href
    
    def start(self):
        html_content = self.get_html_content(self.url)
        if html_content is not None:
            urls = self.crawler(html_content)
            return urls


class SrcCrawler(Base):
    
    def __init__(self, url, start_link=None, attr_tag=None, src_type='src'):
        self.url = url
        self.start_link = start_link
        self.attr_tag = attr_tag
        self.src_type = src_type

    def crawler(self, html_content):
        soup = BeautifulSoup(html_content, 'html.parser')
        content = soup.find_all("img", attrs=self.attr_tag)
        final_src = []
        if self.start_link is not None:
             for a_tag in content:
                final_src.append(self.start_link+a_tag.get(self.src_type))
        else:
            for a_tag in content:
                final_src.append(a_tag.get(self.src_type))
        return final_src
    
    def start(self):
        html_content = self.get_html_content(self.url)
        if html_content is not None:
            src = self.crawler(html_content)
            return src
    
