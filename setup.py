from setuptools import setup


setup(
    name='crawler',
    version='0.1',
    description='Crawl Links And Src From Sites',
    url='#',
    author='mohammadjafa',
    license='MIT', 
    package=['crawler'],
    install_requires=['requests', 'bs4'],
    zip_safe=False
)